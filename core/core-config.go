// список нод для отслеживания состояния доступности
// формируется из dsn-строк , нижеописанного формата
// -------------------------------------------------
//user:password@servername[:port_number]/database_name_or_file[?params1=value1[&param2=value2]...]
//General
//user: login user
//password: login password
//servername: Firebird server's host name or IP address.
//port_number: Port number. default value is 3050.
//database_name_or_file: Database path (or alias name).
//------------------------------------------------------
// как пример : root:rootpassword@172.30.30.30:3055/имя_файла_базы_данных_с_полным_путем_и_расширением.fdb
// часть после собаки (@) это часть должна быть аналогична формату покдлючения к базе посредством ISQL
// через команду CONNECT DATABASE
// проверки корректности формата не делаю, можно добавить позже

package core

import "time"

type Config struct {
	//server section = локальная WEB-морда, вместо консоли
	Global_Server_ip   string `yaml:"global_server_ip"`   // какой интерфейс слушать
	Global_Server_port string `yaml:"global_server_port"` //tcp-порт, на каком принимать входящие
	Color              bool   `yaml:"color"`              //включать использование ansi цветов при выводе или нет

	//node section
	NodeList   []string      `yaml:"nodelist"`   //список серверов для опроса
	NodeTicker time.Duration `yaml:"nodeticker"` // интервал опроса

	//console section
}
