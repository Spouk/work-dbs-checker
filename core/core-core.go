package core

import (
	"database/sql"
	"fmt"
	_ "github.com/nakagami/firebirdsql"
	"gitlab.com/Spouk/gotool/config"
	"log"
	"os"
	"sync"
	"time"
)

const (
	//messages
	prefixConsole         = "[work-dbs-checker] "
	errorReadConfigFile   = "ошибка чтения конфигурационного файла"
	infoWorkerStart       = "worker #%d в работе по dsn = `%s`"
	infoWorkerCheckStatus = "....[ %s ]"
	infoCountWork       = "количество запущенных горутин #%d\n"

	//colors for text output
	ColorBlack  = "\u001b[30m"
	ColorRed    = "\u001b[31m"
	ColorGreen  = "\u001b[32m"
	ColorYellow = "\u001b[33m"
	ColorBlue   = "\u001b[34m"
	ColorReset  = "\u001b[0m"
)

type WorkDBSChecker struct {
	Log *log.Logger
	C   *Config
	wg  *sync.WaitGroup
}

//создание инстанса тулзы
func NewConsole() *WorkDBSChecker {
	//создаю новый инстанс
	ins := &WorkDBSChecker{
		Log: log.New(os.Stdout, prefixConsole, log.LstdFlags),
		C:   &Config{},
		wg:  &sync.WaitGroup{},
	}
	//возвращаю инстанс
	return ins
}

//чтение конфига
func (w *WorkDBSChecker) ReadConfig(filename string) {
	//читаю конфигурационный файл
	c := config.NewConf("", os.Stdout)
	if err := c.ReadConfig(filename, w.C); err != nil {
		w.Log.Fatalf("%s  = `%v`\n", errorReadConfigFile, err)
	}
}

func (w *WorkDBSChecker) RunChecker() {
	w.managerWorker()
}

//запуск консольного менеджера горутин
//берет значения из C.NodeList
//и по каждому значению запускает отдельную горутину
//для взаимодействия создает 1 канал - для передачи горутинам команд
//алгоритм в целом такой: запускается менеджер, по количеству элементов в списке запускает горутин
// длина nodeList = количеству работающих горутин,
//в каждой горутине в цикле устанавливается тикер который берется из конфига и с заданной периодичностью
//делает нужное(ые) действия с нодой - опрашивает, получает/неполучает ответ, анализирует, выводит результат
//анализа в os.stdout, после уходит на очередую итерацию,
//при получении команды из канала от менеджера, обрабатывает ее (это вероятнее всего будет либо завершение работы)
//!!! менеджер нужен только в версии, когда используется WEB морда, тогда основной тред будет висеть за http-сервером,
//	  а менеджер будет работать как горутина, отличие будут в запуске
//    в консольной как запуск функции, с ВЕБмордой как горутина
func (w *WorkDBSChecker) managerWorker() {
	//создаю канал для передачи команд
	ch := make(chan string)
	var count  = 0
	//запускаю горутины по спиcку нод
	for x := 0; x < len(w.C.NodeList); x++ {
		w.wg.Add(1)
		go w.worker(ch, w.C.NodeList[x], x)
		count += 1
	}
	w.Log.Printf("запущенно горутин: #%d  Период обновления: %d минут(а/ы)\n", count, w.C.NodeTicker)
	w.wg.Wait()
}

//реализация горутины-рабочего
func (w *WorkDBSChecker) worker(chCommand chan string, node string, workerNumber int) {
	//w.Log.Printf(fmt.Sprintf(infoWorkerStart, workerNumber, node))
	defer func() {
		w.wg.Done()
	}()
	ticker := time.NewTicker(w.C.NodeTicker * time.Minute)
	for {
		select {
		//канал от менеджера
		case s := <-chCommand:
			switch s {
			case "quit":
				return
			}
			//ждем события и запускаем проверку сервера
		case <-ticker.C:
			//создаю коннектор к базе данных
			conn, err := sql.Open("firebirdsql", node)
			if err != nil {
				if w.C.Color {
					w.Log.Printf("%s %s\n", w.wrapColor(ColorRed, "[ERROR]"), "ошибка при создании подключения к базе данных")
				} else {
					w.Log.Printf("%s %s\n", "[ERROR]", "ошибка при создании подключения к базе данных")
				}

			} else {
				//чекаю бд на доступность по коннекту
				if err = conn.Ping(); err != nil {
					//бд лежит
				}
				//делаю выборку из базы данных
				var n int
				conn.QueryRow("select  first 1 MON$SERVER_PID  from MON$ATTACHMENTS;").Scan(&n)
				if n > 0 {
					//продолжить работу ветки, без дальнейшей обработки
					continue

					//база доступна
					if w.C.Color {
						w.Log.Printf("%s %s\n", w.wrapColor(ColorGreen, "[OK]"), fmt.Sprintf("%v", node))
					} else {
						w.Log.Printf("%s %s\n", "[OK]", node)
					}

				} else {
					//жопа
					if w.C.Color {
						w.Log.Printf("%s %s\n", w.wrapColor(ColorRed, "[FAIL]"), fmt.Sprintf("%v", node))
					} else {
						w.Log.Printf("%s %s\n", "[FAIL]", node)
					}
				}
			}
		}
	}
}

//враппер для придание цвета консольному тексту
func (w *WorkDBSChecker) wrapColor(color, msg string) string {
	return fmt.Sprintf("%s %s %s", string(color), msg, string(ColorReset))
}
