package main

import (
	"flag"
	_ "github.com/nakagami/firebirdsql"
	"gitlab.com/work-dbs-checker/core"
)

const (
	errorMsgFlagFatalDual           = "фатальная ошибка: нельзя запускать приложение с одновременно включенными флагами - server и console"
	errorMsgFlagConfigNofile        = "фатальная ошибка: путь к конфигу не может быть пустым"
	errorMsgFlagConfigErrorOpenFile = "фатальная ошибка:  %v "
	infoMsgDEVOPS                   = "данный функционал в разработке"
)

func main() {

	//создание инстанса тулзы
	app := core.NewConsole()

	//определение переменных под параметры командной строки
	var (
		serverON   = false //запуск в режиме сервера с веб-мордой, авторизацией, и API для получения данных по сформированному запросу
		consoleOn  = true  //запуск консольной версии утилиты, по дефолту
		configFile string  //путь к конфигу для чтения параметров программы
	)
	//определение параметров командной строки
	flag.BoolVar(&consoleOn, "console", true, "запуск утилиты в консольном режиме")
	flag.BoolVar(&serverON, "server", false, "запуск утилиты в серверном режиме")
	flag.StringVar(&configFile, "config", "", "полный путь с именем файла конфигурации")
	//проверка параметров командной строки
	flag.Parse()

	if len(configFile) == 0 {
		app.Log.Fatalln(errorMsgFlagConfigNofile)
	}
	if serverON && consoleOn {
		app.Log.Fatalln(errorMsgFlagFatalDual)
	}
	//в разработке, сервер
	if serverON && consoleOn == false {
		app.Log.Printf(infoMsgDEVOPS)
	}
	//консольная версия
	if serverON == false && consoleOn {
		//открываю конфиг
		app.ReadConfig(configFile)
		//app.Log.Printf("CONFIG: %v\n", app.C)
		//запуск менеджера
		app.RunChecker()
	}
}
