#!/bin/sh

GOOS=linux GOARCH=amd64 go build -o dbs-firebird-checker-linux64 dbs-firebird-checker.go
GOOS=windows GOARCH=amd64 go build -o dbs-firebird-checker-win64.exe dbs-firebird-checker.go
